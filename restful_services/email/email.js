/* packages*/

var AWS = require('aws-sdk');
// var s3 = new aws.S3({ apiVersion: '2006-03-01' });
var jsonfile = require('jsonfile');
var mysql = require('mysql');
AWS.config.update({ region: 'us-west-2' });
var format = require('date-format');
// get reference to S3 client
var s3 = new AWS.S3();
//SES
var ses = new AWS.SES();
var config = require('./config.js');


/******************* INSTANCE VARIABLES ******************/

var express = require('express');
var router = express.Router();
var db = require('./../config/db_gl.js');


//MYSQL CONNECTION
var connection = mysql.createConnection({
    host: 'poduat.translytix.com',
    user: 'poduser',
    password: 'Passw0rd1',
    database: 'pod_uat'
});

// connection.connect()
var params = {
    SOURCE_BUCKET: 'poduat-mobile',
    DEST_BUCKET: 'poduat-prod'
};

var response;
var subject;
var toAddress = []
toAddressWelcome = []
var adminPersons = []

router.post('/sendGeneralMail', function(req, res) {
    mailType = 'general';
    var json = req.body;
    toAddress = json.toAddress
    subject = json.subject
    res.send(sendEmail(json))
})

router.post('/register', function(req, res) {
    toAddress = []
    toAddressWelcome = []
    response = res;
    mailType = 'reg';
    var json = req.body;
    subject = json.status;
    var res_result = '';
    res_result_BD_BidUser = ''
    var sql = "SELECT * FROM BD_BidUser bu where bu.email = '" + json.email + "'";
    showlog(sql);
    toAddressWelcome.push(json.email);
    toAddress.push(json.email)
    connection.query(sql, function(err, result) {
        showlog(result)
        res_result_BD_BidUser = result[0];
        var event = {}
        if ((toAddressWelcome.length > 0) && (json.status == 'REQUEST_BID_ACCESS')) {
            var sql = "SELECT * FROM BD_Notification bn where bn.keyId = '" + json.status + "'";
            connection.query(sql, function(err, result) {
                var sql = "SELECT * FROM BD_Notification bn where bn.keyId = '" + json.status + "'";
                connection.query(sql, function(err, resultShipperDoc) {
                    mailType = 'REQUEST_BID_ACCESS'
                    subject = result[0].subject
                    event.Notification = result[0]
                        // event.bidUser = res_result_BD_BidUser
                    event.shipperDoc = resultShipperDoc[0]
	  	    event.shipperDoc.uri==null?'null.jpg':event.shipperDoc
                    sendWelcomeEmail(event)
                })
                var sql = "SELECT * FROM `view_user_role` WHERE BIDmanageBidsEmail = 1";
                showlog(sql);
                connection.query(sql, function(err, result) {
                    mailType = 'ACCEPTDENY'
                    adminPersons = []
                    toAddress = []
                    showlog(JSON.stringify(result));
                    for (i = 0; i < result.length; i++) {
                        if (json.status == 'REQUEST_BID_ACCESS') {
                            adminPersons.push(result[i].email)
                            toAddress = adminPersons
                                // adminPersons = []
                        } else {
                            adminPersons = []
                        }
                        showlog(adminPersons)
                        if (i == result.length - 1) {
                            adminPersons = []

                            res.send(sendAcceptDenyEmail(res_result_BD_BidUser))
                                //res.send(sendAcceptDenyEmail(res_result_BD_BidUser))
                        }
                    }
                })
            })

        } else if ((json.status == 'ACCEPT_BID_ACCESS') || (json.status == 'DENY_BID_ACCESS')) {
            var sql = "SELECT * FROM BD_Notification bn where bn.keyId = '" + json.status + "'";
            connection.query(sql, function(err, result) {
                    subject = result[0].subject
                    var event = {}
                    if (json.status == 'ACCEPT_BID_ACCESS') {
                        var sql = "SELECT * FROM BD_Notification bn where bn.keyId = '" + json.status + "'";
                        connection.query(sql, function(err, resultShipperDoc) {
                            mailType = 'ACCEPT_BID_ACCESS'
                            event.Notification = result[0]
                            event.bidUser = res_result_BD_BidUser
                            event.shipperDoc = resultShipperDoc[0]
			    event.shipperDoc.uri==null?'null.jpg':event.shipperDoc
                            res.send(sendWelcomeEmail(event))
                        })
                    } else {
                        var sql = "SELECT * FROM BD_Notification bn where bn.keyId = '" + json.status + "'";
                        connection.query(sql, function(err, resultShipperDoc) {
                            mailType = 'REQUEST_BID_ACCESS'
                            event.Notification = result[0]
                            event.bidUser = res_result_BD_BidUser
                            event.shipperDoc = resultShipperDoc[0]
			    event.shipperDoc.uri==null?'null.jpg':event.shipperDoc
                            res.send(sendWelcomeEmail(event))
                        })
                    }

                })
                // var sql = "SELECT * FROM `view_user_role` WHERE BIDmanageBidsEmail = 1";
                // showlog(sql);
                // connection.query(sql, function(err, result) {
                //     mailType = 'ACCEPT_BID_ACCESS'
                //     subject = ''
                //     adminPersons = []
                //     for (i = 0; i < result.length; i++) {
                //         adminPersons = []
                //         if (i == result.length - 1) {
                //             res.send(checkValue(res_result_BD_BidUser))
                //         }
                //     }
                // })
        }
    });
});

router.post('/forgotaccessmail', function(req, res) {
    toAddress = []
    response = res;
    mailType = 'reg';
    var json = req.body;
    subject = json.status;
    var res_result = '';
    //var sql = "UPDATE BD_BidUser SET givenCode='" + json.givenCode + "' WHERE email = '" + json.email + "'";
    showlog(sql);
    toAddress.push(json.email);
    adminPersons = []
	showlog("------------------------------------------------------------------------------------------------------")
    var sql = "SELECT * FROM BD_BidUser where email = '" + json.email + "'";
    showlog(sql);
    connection.query(sql, function(err, result) {
showlog("------------------------------------------------------------------------------------------------------")

	showlog(result)
showlog("------------------------------------------------------------------------------------------------------")

        res_result = result[0];
        showlog("::::::::::::::::::::::::::::::::::::::::")
        showlog(result[0].active)
        showlog("::::::::::::::::::::::::::::::::::::::::")
        if (result[0].active == '2') {
            showlog("IN IF")
            var event = {}
            mailType = 'forgotmailaccesspending'
            var sql = "SELECT * FROM BD_Notification where keyId = 'FORGOT_ACCESS_STATUS_PEND'";
            connection.query(sql, function(err, result) {
                showlog(result[0])
                event = result[0]
                res.send(sendEmail(event))
            })
        } else if (result[0].active == '0') {
            showlog("IN IF")
            var event = {}
            mailType = 'forgotmailaccesspending'
            var sql = "SELECT * FROM BD_Notification where keyId = 'FORGOT_ACCESS_STATUS_DENY'";
            connection.query(sql, function(err, result) {
                showlog(result[0])
                event = result[0]
                res.send(sendEmail(event))
            })
        } else {
            res.send(checkValue(res_result))
        }

    })

});


router.post('/makebid', function(req, res) {
    response = res;
    mailType = 'makeBid';
    subject = "New Bid"
    var json = req.body;
    var res_result = '';
    showlog(json.id)
        // var sql = "SELECT * FROM BD_Bid where id = " + json.id;
    var sql = "SELECT b.*,c.name FROM BD_Bid b LEFT JOIN BD_Carrier c USING(carrierId) WHERE id =" + json.id;
    showlog(sql);

    connection.query(sql, function(err, result) {
        if (err) {
            showlog("BID ERR======>>")
            showlog(err)
        }
        showlog("BID======>>")
        showlog(result)
        showlog(result[0].bidUserId)
        res_result = result[0];
        showlog("RESULT TO SHOW::: " + JSON.stringify(res_result))
        var userMailSql = "SELECT * FROM BD_BidUser where id = " + result[0].bidUserId;
        connection.query(userMailSql, function(err, result1) {
                toAddress = []
                toAddress.push(result1[0].email);
                showlog("USERS::" + toAddress)
                showlog("RESULT::" + res_result)
                if (res_result) {
                    var sql = "SELECT * FROM `view_user_role` WHERE BIDmanageBidsEmail = 1";
                    showlog(sql);
                    connection.query(sql, function(err, result) {
                        adminPersons = []
                        showlog(JSON.stringify(result));
                        for (i = 0; i < result.length; i++) {
                            adminPersons.push(result[i].email)
                            showlog(adminPersons)
                            if (i == result.length - 1) {
                                showlog("Sending Email........")
                                res.send(checkValue(res_result))
                            }
                        }
                    })
                }
            })
            // if (err) callback(err);
            //res.send("Mail Sent Successfully")
    });
});

//export this router to use in our index.js

var folderToCreate;

var obj;
var flepath
var fname
var ftype
var fsize
var fthumbnailurl
var furl
var mailType
var bucket, key;

function sendEmail(eventy) {
    showlog('inside sendEmail...' + jsontostring(eventy));
    // Read the template file
    if (mailType == 'reg') {
        bucket = config.templateBucket;
        key = config.templateKey
    } else if (mailType == 'makeBid') {
        bucket = config.bidTemplateBucket;
        key = config.bidTemplateKey
    } else if (mailType == 'REQUEST_BID_ACCESS') {
        bucket = config.templateBucket;
        key = config.bidUserRegnTemplateKey
    } else if (mailType == 'general') {
        bucket = config.templateBucket;
        key = config.generalTemplateKey
    } else if (mailType == 'forgotmailaccesspending') {
        bucket = config.templateBucket;
        key = config.forgotAccessStatusPendingTemplateKey
    }
    s3.getObject({
        Bucket: bucket,
        Key: key
    }, function(err, data) {
        if (err) {
            // Error
            showlog(err, err.stack);
            showlog('Internal Error: Failed to load template from s3.')
        } else {
            var templateBody = data.Body.toString();
            showlog("Template Body: " + templateBody);
            // Perform the substitutions
            var mark = require('markup-js');
            //Email Content Preparations
            var message = mark.up(templateBody, eventy);
            showlog("Final message: " + message);
            if (true) {
                var params = {
                    Destination: {
                        ToAddresses: toAddress,
                        BccAddresses: ['sys_emails@translytix.com']
                    },
                    Message: {
                        Subject: {
                            Data: "Translytix BID - " + subject,
                            Charset: 'UTF-8'
                        }
                    },
                    Source: config.fromAddress,
                    ReplyToAddresses: ['<support@translytix.com>']
                };
                var fileExtension = config.templateKey.split(".").pop();
                if (fileExtension.toLowerCase() == 'html') {
                    params.Message.Body = {
                        Html: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else if (fileExtension.toLowerCase() == 'txt') {
                    params.Message.Body = {
                        Text: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else {
                    showlog('Internal Error: Unrecognized template file extension: ' + fileExtension);
                    return;
                }

                showlog(params);
                // Send the email
                ses.sendEmail(params, function(err, data) {
                    if (err) {
                        showlog(err, err.stack);
                        showlog('Internal Error: The email could not be sent.');
                    } else {
                        showlog(data); // successful response
                        showlog('Email was successfully sent !');
                        //connection.end()
                    }
                });
            }
        }
    });
}

function sendWelcomeEmail(eventy) {
    showlog('inside sendEmail...' + jsontostring(eventy));
    // Read the template file
    if (mailType == 'reg') {
        bucket = config.templateBucket;
        key = config.templateKey
    } else if (mailType == 'makeBid') {
        bucket = config.bidTemplateBucket;
        key = config.bidTemplateKey
    } else if (mailType == 'REQUEST_BID_ACCESS') {
        bucket = config.templateBucket;
        key = config.bidUserRegnTemplateKey
    } else if (mailType == 'ACCEPT_BID_ACCESS') {
        bucket = config.templateBucket;
        key = config.bidUserAcceptTemplateKey
    }
    s3.getObject({
        Bucket: bucket,
        Key: key
    }, function(err, data) {
        if (err) {
            // Error
            showlog(err, err.stack);
            showlog('Internal Error: Failed to load template from s3.')
        } else {
            var templateBody = data.Body.toString();
            showlog("Template Body: " + templateBody);
            // Perform the substitutions
            var mark = require('markup-js');
            //Email Content Preparations
            var message = mark.up(templateBody, eventy);
            showlog("Final message: " + message);
            if (true) {
                var params = {
                    Destination: {
                        ToAddresses: toAddressWelcome,
                        BccAddresses: ['sys_emails@translytix.com']
                    },
                    Message: {
                        Subject: {
                            Data: "Translytix BID - " + subject,
                            Charset: 'UTF-8'
                        }
                    },
                    Source: config.fromAddress,
                    ReplyToAddresses: ['<support@translytix.com>']
                };
                var fileExtension = config.templateKey.split(".").pop();
                if (fileExtension.toLowerCase() == 'html') {
                    params.Message.Body = {
                        Html: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else if (fileExtension.toLowerCase() == 'txt') {
                    params.Message.Body = {
                        Text: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else {
                    showlog('Internal Error: Unrecognized template file extension: ' + fileExtension);
                    return;
                }

                showlog(params);
                // Send the email
                ses.sendEmail(params, function(err, data) {
                    if (err) {
                        showlog(err, err.stack);
                        showlog('Internal Error: The email could not be sent.');
                    } else {
                        showlog(data); // successful response
                        showlog('Email was successfully sent !');
                        //connection.end()
                    }
                });
            }
        }
    });
}

function sendAcceptDenyEmail(eventy) {
    // showlog('inside sendEmail...' + jsontostring(eventy));
    // // Read the template file
    // //  bucket = config.templateBucket;
    // //     key = config.templateKey
    // s3.getObject({
    //     Bucket: config.templateBucket,
    //     Key: config.templateKey
    // }, function(err, data) {
    //     if (err) {
    //         // Error
    //         showlog(err, err.stack);
    //         showlog('Internal Error: Failed to load template from s3.')
    //     } else {
    //         var templateBody = data.Body.toString();
    //         showlog("Template Body: " + templateBody);
    //         // Perform the substitutions
    //         var mark = require('markup-js');
    //         //Email Content Preparations
    //         var message = mark.up(templateBody, eventy);
    //         showlog("Final message: " + message);
    //         if (true) {
    //             var params = {
    //                 Destination: {
    //                     ToAddresses: toAddress,
    //                     BccAddresses: adminPersons
    //                 },
    //                 Message: {
    //                     Subject: {
    //                         Data: "Translytix BID - " + subject,
    //                         Charset: 'UTF-8'
    //                     }
    //                 },
    //                 Source: config.fromAddress,
    //                 ReplyToAddresses: ['<support@translytix.com>']
    //             };
    //             var fileExtension = config.templateKey.split(".").pop();
    //             if (fileExtension.toLowerCase() == 'html') {
    //                 params.Message.Body = {
    //                     Html: {
    //                         Data: message,
    //                         Charset: 'UTF-8'
    //                     }
    //                 };
    //             } else if (fileExtension.toLowerCase() == 'txt') {
    //                 params.Message.Body = {
    //                     Text: {
    //                         Data: message,
    //                         Charset: 'UTF-8'
    //                     }
    //                 };
    //             } else {
    //                 showlog('Internal Error: Unrecognized template file extension: ' + fileExtension);
    //                 return;
    //             }

    //             showlog(params);
    //             checkValue(params)
    //                 // Send the email
    //                 // ses.sendEmail(params, function(err, data) {
    //                 //     if (err) {
    //                 //         showlog(err, err.stack);
    //                 //         showlog('Internal Error: The email could not be sent.');
    //                 //     } else {
    //                 //         showlog(data); // successful response
    //                 //         showlog('Email was successfully sent !');
    //                 //         //connection.end()
    //                 //     }
    //                 // });
    //         }
    //     }
    // });
    showlog('inside sendEmail...' + jsontostring(eventy));
    // Read the template file
    if (mailType == 'reg') {
        bucket = config.templateBucket;
        key = config.templateKey
    } else if (mailType == 'makeBid') {
        bucket = config.bidTemplateBucket;
        key = config.bidTemplateKey
    } else if (mailType == 'REQUEST_BID_ACCESS') {
        bucket = config.templateBucket;
        key = config.bidUserRegnTemplateKey
    } else if (mailType == 'ACCEPT_BID_ACCESS') {
        bucket = config.templateBucket;
        key = config.bidUserAcceptTemplateKey
    } else if (mailType == 'ACCEPTDENY') {
        bucket = config.templateBucket;
        key = config.bidAdminRegnTemplateKey
    }
    s3.getObject({
        Bucket: bucket,
        Key: key
    }, function(err, data) {
        if (err) {
            // Error
            showlog(err, err.stack);
            showlog('Internal Error: Failed to load template from s3.')
        } else {
            var templateBody = data.Body.toString();
            showlog("Template Body: " + templateBody);
            // Perform the substitutions
            var mark = require('markup-js');
            //Email Content Preparations
            var message = mark.up(templateBody, eventy);
            showlog("Final message: " + message);
            if (true) {
                var params = {
                    Destination: {
                        ToAddresses: toAddress,
                        BccAddresses: ['sys_emails@translytix.com']
                    },
                    Message: {
                        Subject: {
                            Data: "Translytix BID - " + subject,
                            Charset: 'UTF-8'
                        }
                    },
                    Source: config.fromAddress,
                    ReplyToAddresses: ['<support@translytix.com>']
                };
                var fileExtension = config.templateKey.split(".").pop();
                if (fileExtension.toLowerCase() == 'html') {
                    params.Message.Body = {
                        Html: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else if (fileExtension.toLowerCase() == 'txt') {
                    params.Message.Body = {
                        Text: {
                            Data: message,
                            Charset: 'UTF-8'
                        }
                    };
                } else {
                    showlog('Internal Error: Unrecognized template file extension: ' + fileExtension);
                    return;
                }

                showlog(params);
                // Send the email
                ses.sendEmail(params, function(err, data) {
                    if (err) {
                        showlog(err, err.stack);
                        showlog('Internal Error: The email could not be sent.');
                    } else {
                        showlog(data); // successful response
                        showlog('Email was successfully sent !');
                        //connection.end()
                    }
                });
            }
        }
    });
}

function checkValue(param) {
    var resultant = []
    var dq = '"';
    var json = "{";
    var last = Object.keys(param).length;
    var count = 0;
    for (var k in param) {
        if (typeof param[k] !== 'function') {
            const key = k
            showlog("Key is " + k + ", value is" + param[k]);
            if ((k === 'refridgerated') && (param[k] == 0)) {
                param[k] = 'Dry'
            } else if ((k === 'refridgerated') && (param[k] == 1)) {
                param[k] = 'Refrigerated'
            } else if (((k == 'dropTrailersLoadReq') || (k == 'dropTrailersUnlodReq')) && (param[k] == 0)) {
                param[k] = 'No'
            } else if (((k == 'dropTrailersLoadReq') || (k == 'dropTrailersUnlodReq')) && (param[k] == 1)) {
                param[k] = 'Yes'
            } else if (param[k] == null) {
                param[k] = ' '
            }
            json += dq + [key] + dq + ":" + dq + param[k] + dq;
            count++;
            if (count < last)
                json += ",";
        }
    }
    json += "}";
    resultant.push(json)
    sendEmail(JSON.parse(resultant))
}

function showlog(str) {
    console.log(str);
}


function jsontostring(str) {
    return JSON.stringify(str);
}
module.exports = router;