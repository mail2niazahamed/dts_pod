var express = require('express');
var router = express.Router();
var db = require('./../config/db_gl.js');

var response;
router.get('/:table', function(req, res) {
    response = res;
    console.log(req.params.table);
    //db.gets(req, callback);
    db.getsPaginate(req, callback);

});
router.get('/:table/:id', function(req, res) {
    response = res;
    console.log(req.params.table + ',' + req.params.id);
    db.get(req, callback);

});
router.post('/:table', function(req, res) {
    response = res;
    db.post(req, callback);
});
router.delete('/:table/:id', function(req, res) {
    response = res;
    db.delete(req, callback);
});
router.put('/:table/:id', function(req, res) {
    response = res;
    db.put(req, callback);

});
  
function callback(str) {
    response.send(str);
}

//export this router to use in our index.js
module.exports = router;
