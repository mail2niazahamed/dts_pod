var express = require('express');
var router = express.Router();
var db = require('./../config/db.js');

router.get('/', function(req, res) {
    db.getFacilityShipment(req.query.facilityId, function(str) {
        res.send(str);
    });
});

module.exports = router;
