var mysql = require('mysql');
//MYSQL CONNECTION
var connection = mysql.createConnection({
    host: 'poduat.translytix.com',
    // host: 'localhost',
    user: 'poduser',
    password: 'Passw0rd1',
    database: 'pod_uat'
});
var numRows;
const pageLimit = 300
module.exports = {
    getsPaginate: function(req, callback) {
        var queryPagination;
        var numPerPage = parseInt(req.query.limit) || pageLimit;
        var page = parseInt(req.query.page) - 1 || 0;
        var numPages;
        var skip = page * numPerPage;
        var limit = skip + ',' + numPerPage;
        if (req.query.activeYN) {
            sql = "select count(*) as numRows from " + req.params.table + " WHERE active=1";
        } else {
            sql = "select count(*) as numRows from " + req.params.table;
        }
        if (page == 0) {
            connection.query(sql, function(err, results) {
                numRows = results[0].numRows;
                console.log('number of rows:', numRows);
                numPages = Math.ceil(numRows / numPerPage);
                console.log('number of pages:', numPages);
                var res_result = '';
                if (req.query.activeYN) {
                    sql = 'SELECT * FROM ' + req.params.table + " WHERE active=1 LIMIT " + limit;
                } else {
                    sql = 'SELECT * FROM ' + req.params.table + " LIMIT " + limit;
                }
                console.log(sql)
                connection.query(sql, function(err, result) {
                    if (err) callback(err);
                    res_result = {
                        'count': numRows,
                        'page': req.query.page,
                        'limit': req.query.limit,
                        'data': result
                    }
                    callback(JSON.stringify(res_result));
                });
            })
        } else {
            var res_result = '';
            if (req.query.activeYN) {
                sql = 'SELECT * FROM ' + req.params.table + " WHERE active=1 LIMIT " + limit;
            } else {
                sql = 'SELECT * FROM ' + req.params.table + " LIMIT " + limit;
            }
            console.log(sql)
            connection.query(sql, function(err, result) {
                if (err) callback(err);
                res_result = {
                    'count': numRows,
                    'page': req.query.page,
                    'limit': req.query.limit,
                    'data': result
                }
                callback(JSON.stringify(res_result));
            });
        }
    },

    /*gets: function(req, callback) {
        console.log(req.query.days)
        var daysLimit = req.query.days
        sql = 'SELECT * FROM ' + req.params.table;
        var query = connection.query(sql, req, function(err, result) {
            if (err) callback(err);;
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
        console.log(query.sql);
    },*/
    get: function(req, callback) {
        console.log('............' + req.params.table + ',' + req.params.id);
        sql = "SELECT * FROM " + req.params.table + " WHERE id  = '" + req.params.id + "'";
        var query = connection.query(sql, req, function(err, result) {
            if (err) callback(err);;
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
        console.log(query.sql);
    },
    post: function(req, callback) {
        sql = 'INSERT INTO ' + req.params.table + ' SET ?';
        var query = connection.query(sql, req.body, function(err, result) {
            if (err) callback(err);;
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
        console.log(query.sql);
    },
    delete: function(req, callback) {
        sql = 'DELETE FROM ?? WHERE id = ?';
        var inserts = [req.params.table, req.params.id];
        sql = mysql.format(sql, inserts);
        var query = connection.query(sql, req, function(err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
        console.log(query.sql);
    },
    put: function(req, callback) {
        var res_result = '';
        sql = 'update ?? set ? WHERE id = ?';
        var update = [req.params.table, req.body, req.params.id];
        sql = mysql.format(sql, update);
        console.log(sql)
        var query = connection.query(sql, function(err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
        console.log(query.sql);
    }
};