var mysql = require('mysql');
//MYSQL CONNECTION
var connection = mysql.createConnection({
    host: 'poduat.translytix.com',
    // host: 'localhost',
    user: 'poduser',
    password: 'Passw0rd1',
    database: 'pod_uat'
});
var numRows;
const pageLimit = 300
module.exports = {
    userLogin: function (useremail, pwd, callback) {
        var res_result = '';
        sql = "SELECT * FROM users WHERE email='" + useremail + "' and pwd='" + pwd + "'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            if (result.length > 0) {
                console.log(JSON.stringify(result));
                var reslt = result
                res_result = JSON.stringify(result);
                sql = "SELECT * FROM role WHERE id='" + result[0].role + "'";
                console.log(sql)
                connection.query(sql, function (err, result) {
                    var bid = '';
                    var userMenu = '';
                    var urlRedir = '';
                    var superAdmin = '';
                    var res_role_result = JSON.stringify(result);
                    if (result[0].superadmin == 1) {
                        userMenu = 'admin'
                        superAdmin = true
                        bid = true
                        urlRedir = '../users'
                    } else if ((result[0].managePOD == 1) && (result[0].manageBID == 1)) {
                        userMenu = 'admin'
                        bid = true
                        superAdmin = false
                        urlRedir = '../shipment'
                    } else if (result[0].managePOD == 1) {
                        userMenu = 'podadmin'
                        bid = false
                        superAdmin = false
                        urlRedir = '../shipment'
                    } else if (result[0].manageBID == 1) {
                        userMenu = 'bidadmin'
                        bid = true
                        superAdmin = false
                        urlRedir = '../uploadlanes'
                    }
                    var res_results = {
                        'userMenu': userMenu,
                        'bid': bid,
                        'superAdmin': superAdmin,
                        'urlRedir': urlRedir,
                        'data': reslt
                    }
                    console.log(userMenu)
                    callback(JSON.stringify(res_results));
                })
            } else {
                var res_results = {
                    data: result
                }
                callback(JSON.stringify(res_results));
            }
        });
    },

    getUser: function (req, callback) {
        var res_result = '';
        sql = "SELECT * FROM users WHERE email='" + req + "'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    getCarrierUsers: function (req, callback) {
        console.log(req)
        console.log(req[0])
        console.log(JSON.stringify(req))
        var res_result = '';
        sql = "SELECT * FROM BD_BidUser WHERE carrierId=" + req;
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    getVerbiage: function (callback) {
        var res_result = '';
        sql = "SELECT * FROM BD_NewUserKey WHERE active=1";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    userforgotPwd: function (req, callback) {
        console.log(req)
        var res_result = '';
        sql = "SELECT * FROM BD_BidUser WHERE email='" + req + "'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    getCarrier: function (id, callback) {
        var res_result = '';
        sql = 'SELECT * FROM BD_Carrier WHERE name="' + id + '"';
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    bidUserLogin: function (useremail, pwd, callback) {
        var res_result = '';
        sql = "SELECT id, bidUserName, profileMatch, carrierId, CASE WHEN givenCode = '" + pwd + "' THEN  givenCode ELSE NULL END AS givencode, CASE WHEN active =  1 THEN active ELSE NULL END AS active FROM BD_BidUser WHERE email='" + useremail + "';"
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            res_result = JSON.stringify(result)
            console.log(res_result)
            callback(res_result);
        })

        // var res_result = '';
        // sql = "SELECT * FROM BD_BidUser WHERE email='" + useremail + "' and givenCode='" + pwd + "'";
        // console.log(sql)
        // connection.query(sql, function(err, result) {
        //     if (err) callback(err);
        //     console.log("USER::::::::" + JSON.stringify(result))
        //     var res_result = (result);
        //     console.log("USER::::::::" + res_result.length)
        //     if (res_result.length > 0) {
        //         if ((result[0].active == 1)) {
        //             sql = "SELECT * FROM BD_BidUser WHERE email='" + useremail + "' and givenCode='" + pwd + "' and active=1";
        //             connection.query(sql, function(err, result) {
        //                 if (err) callback(err)
        //                 console.log(JSON.stringify(result));
        //                 res_result = JSON.stringify(result);
        //                 callback(res_result);
        //             })
        //         } else {
        //             console.log(JSON.stringify(result));
        //             res_result = JSON.stringify(result);
        //             callback(res_result);
        //         }
        //     } else {
        //         callback(res_result);
        //     }
        // });
    },

    getDepotUsers: function (id, callback) {
        var res_result = '';
        //sql = "SELECT * FROM view_depot_user WHERE depot_id='" + id + "'";
        if (id) {
            sql = "SELECT * FROM `view_depot_user` WHERE `depot_id` IN (" + id + ")"
            console.log(sql)
            connection.query(sql, function (err, result) {
                if (err) callback(err);
                console.log(JSON.stringify(result));
                res_result = JSON.stringify(result);
                callback(res_result);
            });
        } else {
            callback(res_result);
        }

    },

    searchLaneOpportunities: function (req, callback) {
        var res_result = '';
        sql = "SELECT * from view_bd_lane where " + req.body.qry;
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    searchReviewBids: function (req, callback) {
        var res_result = '';
        sql = "SELECT * from view_bd_review_bids where " + req.body.qry;
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    getMyBids: function (req, callback) {
        var queryPagination;
        console.log(req.query.limit)
        var numPerPage = parseInt(req.query.limit) || pageLimit;
        var page = parseInt(req.query.page) - 1 || 0;
        var numPages;
        var skip = page * numPerPage;
        var limit = skip + ',' + numPerPage;
        var res_result = '';
        sql = "select count(*) as numRows from view_bd_bid where bidUserId = '" + req.query.id + "'";
        if (page == 0) {
            connection.query(sql, function (err, results) {
                numRows = results[0].numRows;
                console.log('number of rows:', numRows);
                numPages = Math.ceil(numRows / numPerPage);
                console.log('number of pages:', numPages);
                var res_result = '';
                sql = "select * from view_bd_bid where bidUserId='" + req.query.id + "' ORDER BY bidDate DESC " + "LIMIT " + limit;
                console.log(sql)
                connection.query(sql, function (err, result) {
                    if (err) callback(err);
                    res_result = {
                        'count': numRows,
                        'page': req.query.page,
                        'limit': req.query.limit,
                        'data': result
                    }
                    callback(JSON.stringify(res_result));
                });
            })
        } else {
            var res_result = '';
            sql = "select * from view_bd_bid where bidUserId='" + req.query.id + "' ORDER BY bidDate DESC " + " LIMIT " + limit;
            console.log(sql)
            connection.query(sql, function (err, result) {
                if (err) callback(err);
                res_result = {
                    'count': numRows,
                    'page': req.query.page,
                    'limit': req.query.limit,
                    'data': result
                }
                callback(JSON.stringify(res_result));
            });
        }
    },

    downloadReviewBids: function (callback) {
        var res_result = '';
        sql = "select * from view_bd_review_bids";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    getFacilityShipment: function (id, callback) {
        var res_result = '';
        sql = "select * from shipment_detail where facilityId='" + id + "'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },
    getShipmentDetail: function (id, callback) {
        var res_result = '';
        sql = "select * from shipment_detail where shipment_id='" + id + "'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },

    searchShipmentPaginate: function (req, callback) {
        var queryPagination;
        var numPerPage = parseInt(req.query.limit) || pageLimit;
        var page = parseInt(req.query.page) - 1 || 0;
        var numPages;
        var skip = page * numPerPage;
        var limit = skip + ',' + numPerPage;
        var daysLimit = req.query.days ? req.query.days : 0;
        var id = req.params.val;
        var facilityId = req.query.facilityId;
        console.log(facilityId)
        if ((facilityId != 'All Facilities')) {
            sql = daysLimit ? ("select count(*) as numRows from view_depot_shipment where facilityId=" + facilityId + " AND search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt") : ('SELECT count(*) as numRows FROM view_depot_shipment')
        } else {
            sql = daysLimit ? ("select count(*) as numRows from view_depot_shipment where search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt") : ('SELECT count(*) as numRows FROM view_depot_shipment')
        }

        if (page == 0) {
            connection.query(sql, function (err, results) {
                numRows = results[0].numRows;
                console.log('number of rows:', numRows);
                numPages = Math.ceil(numRows / numPerPage);
                console.log('number of pages:', numPages);
                var res_result = '';
                if ((facilityId != 'All Facilities')) {
                    sql = daysLimit ? ("select * from view_depot_shipment where facilityId=" + facilityId + " AND search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt DESC LIMIT " + limit) : ("select * from view_depot_shipment where facilityId=" + facilityId + " AND search like '%" + id + "%' ORDER BY upload_dt DESC LIMIT " + limit);
                } else {
                    sql = daysLimit ? ("select * from view_depot_shipment where search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt DESC LIMIT " + limit) : ("select * from view_depot_shipment where search like '%" + id + "%' ORDER BY upload_dt DESC LIMIT " + limit);
                }

                console.log(sql)
                connection.query(sql, function (err, result) {
                    if (err) callback(err);
                    res_result = {
                        'count': numRows,
                        'page': req.query.page,
                        'limit': req.query.limit,
                        'data': result
                    }
                    callback(JSON.stringify(res_result));
                });
            })
        } else {
            var res_result = '';
            if ((facilityId != 'All Facilities')) {
                sql = daysLimit ? ("select * from view_depot_shipment where facilityId=" + facilityId + " AND search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt DESC LIMIT " + limit) : ("select * from view_depot_shipment where facilityId=" + facilityId + " AND search like '%" + id + "%' ORDER BY upload_dt DESC LIMIT " + limit);
            } else {
                sql = daysLimit ? ("select * from view_depot_shipment where search like '%" + id + "%' AND upload_dt  >= DATE_SUB(SYSDATE(), INTERVAL " + daysLimit + " DAY) ORDER BY upload_dt DESC LIMIT " + limit) : ("select * from view_depot_shipment where search like '%" + id + "%' ORDER BY upload_dt DESC LIMIT " + limit);
            }
            console.log(sql)
            connection.query(sql, function (err, result) {
                if (err) callback(err);
                res_result = {
                    'count': numRows,
                    'page': req.query.page,
                    'limit': req.query.limit,
                    'data': result
                }
                callback(JSON.stringify(res_result));
            });
        }
    },
    searchDepotsPaginate: function (req, callback) {
        var queryPagination;
        var numPerPage = parseInt(req.query.limit) || pageLimit;
        var page = parseInt(req.query.page) - 1 || 0;
        var numPages;
        var skip = page * numPerPage;
        var limit = skip + ',' + numPerPage;
        var facilityId = req.query.facilityId;
        if (facilityId == 'All Facilities') {
            sql = ("select count(*) as numRows from view_depot where search like '%" + id + "%'")
        } else {
            sql = ("select count(*) as numRows from view_depot where facilityId=" + facilityId + " AND search like '%" + id + "%'")
        }

        // var daysLimit = req.query.days ? req.query.days : 0;
        var id = req.params.val;

        if (page == 0) {
            connection.query(sql, function (err, results) {
                numRows = results[0].numRows;
                console.log('number of rows:', numRows);
                numPages = Math.ceil(numRows / numPerPage);
                console.log('number of pages:', numPages);
                var res_result = '';
                if (facilityId == 'All Facilities') {
                    sql = ("select * from view_depot where search like '%" + id + "%' LIMIT " + limit);
                } else {
                    sql = ("select * from view_depot where facilityId=" + facilityId + " AND search like '%" + id + "%' LIMIT " + limit);
                }

                console.log(sql)
                connection.query(sql, function (err, result) {
                    if (err) callback(err);
                    res_result = {
                        'count': numRows,
                        'page': req.query.page,
                        'limit': req.query.limit,
                        'data': result
                    }
                    callback(JSON.stringify(res_result));
                });
            })
        } else {
            var res_result = '';
            if (facilityId == 'All Facilities') {
                sql = ("select * from view_depot where search like '%" + id + "%' LIMIT " + limit);
            } else {
                sql = ("select * from view_depot where facilityId=" + facilityId + " AND search like '%" + id + "%' LIMIT " + limit);
            }

            console.log(sql)
            connection.query(sql, function (err, result) {
                if (err) callback(err);
                res_result = {
                    'count': numRows,
                    'page': req.query.page,
                    'limit': req.query.limit,
                    'data': result
                }
                callback(JSON.stringify(res_result));
            });
        }
    },
    getDateValue: function (req, callback) {
        var res_result = '';
        sql = "select * from settings where skey=FILTER_DAYS'";
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },
    getAllPages: function (req, callback) {
        var res_result = '';
        sql = "SELECT page_id, page_name FROM `BD_HelpText` GROUP BY page_id"
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },
    getPageHelpText: function (req, callback) {
        var res_result = '';
        sql = "SELECT * FROM `BD_HelpText` WHERE page_id='" + req.params.pageId + "'"
        console.log(sql)
        connection.query(sql, function (err, result) {
            if (err) callback(err);
            console.log(JSON.stringify(result));
            res_result = JSON.stringify(result);
            callback(res_result);
        });
    },
};