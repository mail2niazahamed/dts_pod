/*
SQLyog Community
MySQL - 5.5.53-0ubuntu0.14.04.1 : Database - dts_pod
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dts_pod` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dts_pod`;

/*Table structure for table `BD_Bid` */

CREATE TABLE `BD_Bid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `laneId` int(11) DEFAULT NULL,
  `fromCity` varchar(100) DEFAULT NULL,
  `fromState` varchar(100) DEFAULT NULL,
  `toCity` varchar(100) DEFAULT NULL,
  `toState` varchar(100) DEFAULT NULL,
  `distanceMiles` int(100) DEFAULT NULL,
  `refType` varchar(20) DEFAULT 'NULL',
  `WayLH` int(11) DEFAULT '0',
  `RTLH` int(11) DEFAULT '0',
  `expediteLH` int(11) DEFAULT '0',
  `dropTrailors` tinyint(1) NOT NULL DEFAULT '0',
  `laneDateFrom` date DEFAULT NULL,
  `laneDateTo` date DEFAULT NULL,
  `bidDateTime` date DEFAULT NULL,
  `refridgerated` tinyint(1) NOT NULL DEFAULT '0',
  `oneWayTeamRate` int(11) DEFAULT '0',
  `addlInfo` varchar(1000) DEFAULT NULL,
  `originPostalCode` mediumtext,
  `destPostalCode` mediumtext,
  `bidUserId` int(11) NOT NULL,
  `roundTripTeamRate` int(11) DEFAULT '0',
  `shipperId` int(11) NOT NULL,
  `carrierId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

/*Data for the table `BD_Bid` */

insert  into `BD_Bid`(`id`,`groupId`,`laneId`,`fromCity`,`fromState`,`toCity`,`toState`,`distanceMiles`,`refType`,`WayLH`,`RTLH`,`expediteLH`,`dropTrailors`,`laneDateFrom`,`laneDateTo`,`bidDateTime`,`refridgerated`,`oneWayTeamRate`,`addlInfo`,`originPostalCode`,`destPostalCode`,`bidUserId`,`roundTripTeamRate`,`shipperId`,`carrierId`) values (1,1,1,'Concord','SFO','Cupertino','CL',480,'REF',150,90,500,0,'2016-12-19','2016-12-29','2016-12-09',0,100,'','1234','1234',1,0,0,123),(3,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',788,33,33,33,'2016-12-19','2016-12-29','2016-12-09',1,100,'','1234','1234',4,0,0,0),(4,1,1,'Amsterdam','NY','Concord','SFO',600,NULL,NULL,NULL,NULL,0,'2016-12-07','2016-12-07','2016-12-12',1,100,'','1234','1234',4,0,0,0),(5,1,1,'Concord','SFO','Cupertino','CL',480,'REF',1500,900,555,6,'2016-12-19','2016-12-29','2016-12-09',1,100,'','1234','1234',1,0,0,123),(6,1,1,'Amsterdam','NY','Concord','SFO',600,'HELLO',200,3,3,0,'2016-12-07','2016-12-07','2016-12-17',1,100,'','1234','1234',0,0,0,0),(7,1,1,'Concord','SFO','Cupertino','CL',480,'REF',1500,900,555,6,'2016-12-19','2016-12-29','2016-12-09',1,100,'','1234','1234',1,0,0,123),(9,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',3,4,5,1,'2016-12-07','2016-12-07','2016-12-17',1,100,'','1234','1234',1,0,0,0),(10,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2,4,10,1,'2016-12-07','2016-12-07','2016-12-19',0,100,'','1234','1234',1,0,0,0),(11,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',200,150,10,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(12,1,2,'Concord','SFO','Cupertino','CL',480,'REF',200,300,2,1,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(13,1,2,'Concord','SFO','Cupertino','CL',480,'REF',200,300,2,1,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(14,1,2,'Concord','SFO','Cupertino','CL',480,'REF',200,300,2,1,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(15,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',3,3,3,1,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(16,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',4,4,4,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(17,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',4,4,4,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(18,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',34,3,3,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(19,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',1,2,3,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(20,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',2,2,2,2,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(21,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',300,250,30,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(22,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',250,300,10,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(23,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',250,300,10,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(24,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2250,3002,102,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(25,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2250,3002,102,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(26,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2250,3002,102,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(27,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2250,3002,102,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(28,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',300,3,2,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(29,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',300,3,2,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(30,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',300,3,2,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(31,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',300,3,2,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(32,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',300,3,2,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(33,1,1,'Amsterdam','NY','Concord','SFO',600,'3',3,3,3,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(34,1,1,'Amsterdam','NY','Concord','SFO',600,'4',4,4,4,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(35,1,1,'Amsterdam','NY','Concord','SFO',600,NULL,NULL,NULL,NULL,0,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(36,1,1,'Amsterdam','NY','Concord','SFO',600,'4',4,4,4,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(37,1,1,'Amsterdam','NY','Concord','SFO',600,NULL,45,NULL,NULL,0,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(38,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',5,5,5,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(39,1,2,'Concord','SFO','Cupertino','CL',480,NULL,NULL,NULL,NULL,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(40,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',5,5,5,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(41,1,2,'Concord','SFO','Cupertino','CL',480,NULL,NULL,NULL,NULL,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(42,1,2,'Concord','SFO','Cupertino','CL',480,'DRY',5,5,5,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(43,1,2,'Concord','SFO','Cupertino','CL',480,NULL,NULL,NULL,NULL,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(44,1,1,'Amsterdam','NY','Concord','SFO',600,'REF',2,4,6,1,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(45,1,2,'Concord','SFO','Cupertino','CL',480,'34',33,32,31,0,'2016-12-19','2016-12-29','2016-12-19',1,100,'','1234','1234',1,0,0,0),(46,1,1,'Amsterdam','NY','Concord','SFO',600,'DRY',250,260,3,0,'2016-12-07','2016-12-07','2016-12-19',1,100,'','1234','1234',1,0,0,0),(48,1,NULL,'Amsterdam','NY','Concord','SFO',600,'Dry',450,450,450,0,'2016-12-07','2016-12-07','2016-12-23',1,450,'','1234','1234',1,450,0,0);

/*Table structure for table `BD_BidUser` */

CREATE TABLE `BD_BidUser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessName` varchar(500) DEFAULT 'NULL',
  `bidUserName` varchar(500) DEFAULT 'NULL',
  `phoneNumber` mediumtext,
  `email` varchar(100) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `givenCode` varchar(100) DEFAULT 'NULL',
  `profileMatch` tinyint(1) DEFAULT NULL,
  `requestDate` date DEFAULT NULL,
  `carrierId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

/*Data for the table `BD_BidUser` */

insert  into `BD_BidUser`(`id`,`businessName`,`bidUserName`,`phoneNumber`,`email`,`active`,`givenCode`,`profileMatch`,`requestDate`,`carrierId`) values (1,'Business Name','user anem','8888888888','manikandan@constient.com',1,'ABC123',1,'2016-12-12',0),(2,'Sample Name','User2','9999999999','mani@mail.com',1,'ABC321',1,'2016-01-03',0),(3,'BIZ','Mani','9080977440','biz@mail.com',1,'BIZ01',1,'2016-12-09',0),(4,'PeppridgeFarm','SteveGrow','1234567890','steve_grow@pepperidgefarm.com',1,'ABC123',1,'2016-12-07',0),(5,'New Carrier','Mani','909099090','mani@exblz.com',1,'w9x',1,'2016-12-17',0),(6,'Sample','Senthil','9999999999','senthilkraju@gmail.com',1,'abc123',1,'2016-12-20',0),(7,'NEW','NEW','99999999','new@new.com',1,'gmwhxk',NULL,'2016-12-17',0),(47,'CarrierName9191','NiazC','12341234','niaz@constient.com',1,'chr905',1,'2016-12-17',0),(60,'Mani','mani','9988778898','mani',1,'5y05vy',1,'2016-12-19',0),(61,'HELLO','HELLO','HELLO','HELLO',1,'fzcjys',NULL,'2016-12-21',0),(62,'','','','',1,'B8Z2B2',NULL,'2016-12-21',0);

/*Table structure for table `BD_CARRIER` */

CREATE TABLE `BD_CARRIER` (
  `carrierId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_CARRIER` */

insert  into `BD_CARRIER`(`carrierId`,`name`,`active`) values (0,'DTS',1);

/*Table structure for table `BD_COMPANY` */

CREATE TABLE `BD_COMPANY` (
  `companyId` int(11) DEFAULT NULL,
  `policyId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_COMPANY` */

/*Table structure for table `BD_Destination` */

CREATE TABLE `BD_Destination` (
  `id` varchar(100) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postalCode` varchar(6) DEFAULT 'NULL'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_Destination` */

insert  into `BD_Destination`(`id`,`city`,`state`,`postalCode`) values ('NY01','Amsterdam','NY','NULL'),('NY02','Hornell','NY','NULL'),('SF01','Concord','SFO','NULL'),('CL01','Cupertino','CL','NULL');

/*Table structure for table `BD_Group` */

CREATE TABLE `BD_Group` (
  `groupId` int(11) DEFAULT NULL,
  `groupName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_Group` */

/*Table structure for table `BD_Lane` */

CREATE TABLE `BD_Lane` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromDestinationId` varchar(100) NOT NULL DEFAULT 'NULL',
  `toDestinationId` varchar(100) NOT NULL DEFAULT 'NULL',
  `laneDateFrom` date DEFAULT NULL,
  `laneDateTo` date DEFAULT NULL,
  `distanceMiles` int(11) DEFAULT NULL,
  `policyId` int(11) DEFAULT NULL,
  `laneId` int(11) NOT NULL,
  `annualLineHaul` int(11) DEFAULT '0',
  `needRefridgerated` tinyint(1) NOT NULL DEFAULT '0',
  `teamRate` tinyint(1) NOT NULL DEFAULT '0',
  `bidSingleOneWayRate` tinyint(1) NOT NULL DEFAULT '1',
  `bidSingleRoundTripRate` tinyint(1) NOT NULL DEFAULT '1',
  `additionalInfo` varchar(1000) NOT NULL DEFAULT 'NULL',
  `shipperId` int(11) NOT NULL,
  `truckType` varchar(100) DEFAULT 'NULL',
  `daysOfWeek` varchar(255) DEFAULT 'NULL',
  `bidTeamOneWayRate` tinyint(1) NOT NULL DEFAULT '1',
  `bidTeamRoundTripRate` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`fromDestinationId`,`toDestinationId`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `BD_Lane` */

insert  into `BD_Lane`(`id`,`fromDestinationId`,`toDestinationId`,`laneDateFrom`,`laneDateTo`,`distanceMiles`,`policyId`,`laneId`,`annualLineHaul`,`needRefridgerated`,`teamRate`,`bidSingleOneWayRate`,`bidSingleRoundTripRate`,`additionalInfo`,`shipperId`,`truckType`,`daysOfWeek`,`bidTeamOneWayRate`,`bidTeamRoundTripRate`,`active`) values (1,'NY01','SF01','2016-12-07','2016-12-07',600,1,0,0,0,0,1,1,'NULL',0,'NULL','NULL',1,1,1),(2,'SF01','CL01','2016-12-19','2016-12-29',480,1,0,0,0,0,1,1,'NULL',0,'NULL','NULL',1,1,1);

/*Table structure for table `BD_SHIPPER` */

CREATE TABLE `BD_SHIPPER` (
  `shipperId` int(11) NOT NULL,
  `policyId` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_SHIPPER` */

insert  into `BD_SHIPPER`(`shipperId`,`policyId`,`name`) values (1,1,'Pepperidge Farm');

/*Table structure for table `BD_ShipperPolicy` */

CREATE TABLE `BD_ShipperPolicy` (
  `policyId` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_ShipperPolicy` */

insert  into `BD_ShipperPolicy`(`policyId`,`description`) values (1,'Your rates do not include fuel charges\nDistance miles are estimates, and your quotes are estimates.');

/*Table structure for table `BD_cnBidUserBid` */

CREATE TABLE `BD_cnBidUserBid` (
  `bidId` int(11) DEFAULT NULL,
  `bidUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_cnBidUserBid` */

/*Table structure for table `BD_cnProviderUser` */

CREATE TABLE `BD_cnProviderUser` (
  `providerId` int(11) DEFAULT NULL,
  `providerUserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `BD_cnProviderUser` */

/*Table structure for table `depot` */

CREATE TABLE `depot` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address_line_1` varchar(100) DEFAULT NULL,
  `address_line_2` varchar(100) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `county` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `latlng` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `depot` */

insert  into `depot`(`id`,`name`,`address_line_1`,`address_line_2`,`zipcode`,`county`,`city`,`state`,`country`,`phone`,`mobile`,`latlng`,`email`) values ('0000080026','ABERDEEN - BAKERY DEPOT','Addr1','Addr2',NULL,NULL,NULL,NULL,NULL,'\n9 87654324     ',NULL,NULL,NULL),('0000080029','Goldsboro - Bakery Depot','Goldsboro',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0000080636','Orlando #3 Bakery Depot','Orlando',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0000080637','Tampa Bakery Depot','Tampa',NULL,'33618',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0000080663','Vero Beach','Vero Beach',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('0000086936','TESTING - BAKERY DEPOT','ADDR LINE 1','ADDR LINE 2','10001','Saint Rose','Saint Rose','LA','US','123456789','987654321',NULL,'support@dts.com'),('00803651','AUSTIN - BAKERY DEPOT','\n St Luid Rod ','Opp to Rey','10001','Saint Rose','Saint Rose','LA','US','123456789','987654321',NULL,'support@austin.com'),('00803652','HALTOM CITY - BAKERY DEPOT','ADDR LINE 1','ADDR LINE 2','10001','Saint Rose','Saint Rose','LA','US','123456789','987654321',NULL,'support@HALTOM.com'),('00803653','BATON ROUGE - BAKERY DEPOT','ADDR LINE 1','ADDR LINE 2','10001','Saint Rose','Saint Rose','LA','US','123456789','987654321',NULL,'support@BATON.com'),('00803654','GARLAND - BAKERY DEPOT','ADDR LINE 1','ADDR LINE 2','10002','Saint Rose','Saint Rose','LA','US','123456789','987654321',NULL,'support@GARLAND.com'),('123','123','123','dd','4654','54','5','54','5','4','5',NULL,'123@123.com'),('Austin ','Austin ',NULL,NULL,NULL,NULL,NULL,'TX',NULL,NULL,NULL,NULL,NULL),('Baton Rouge ','Baton Rouge ',NULL,NULL,NULL,NULL,NULL,'LA ',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `depot_user` */

CREATE TABLE `depot_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `depot_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `thumbnail_url` text,
  `image_url` text,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `depot_user` (`depot_id`),
  CONSTRAINT `depot_user` FOREIGN KEY (`depot_id`) REFERENCES `depot` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

/*Data for the table `depot_user` */

insert  into `depot_user`(`id`,`depot_id`,`name`,`email`,`mobile`,`thumbnail_url`,`image_url`,`status`) values (1,'00803652','Niaz','mail2niazahamed@gmail.com','9486090108',NULL,NULL,1),(5,'00803651','David','reynolds30062@gmail.com','4044352555',NULL,NULL,1),(6,'00803651','Senthil','senthil.raju@excelblaze.com','',NULL,NULL,1),(7,'00803651','Steve','steve_grow@pepperidgefarm.com',NULL,NULL,NULL,1),(8,'00803651','SS','sstowers@dtsolutions.net',NULL,NULL,NULL,1),(9,'00803651','Brad','bradford_charles@pepperidgefarm.com',NULL,NULL,NULL,1),(10,'0000086936','Senthil','senthil.raju@excelblaze.com','9876543210',NULL,NULL,1),(11,'0000086936','Senthil','senthilkraju@gmail.com','9876543210',NULL,NULL,1),(13,'0000086936','Mani','mani@mani.com','90809788778',NULL,NULL,NULL),(15,'123','roboto','sd@gmail.com','222',NULL,NULL,1),(22,'0000080026','Senthil','senthil.raju@excelblaze.com','9876543210',NULL,NULL,1),(23,'0000080026','Senthil','senthilkraju@gmail.com','9876543210',NULL,NULL,1),(24,'0000080026','Val','val.raman@excelblaze.com','9876543210',NULL,NULL,1),(35,'0000080029','Scott Stowers','sstowers@dtsolutions.net',NULL,NULL,NULL,1),(36,'0000080029','David Reynolds','reynolds30062@gmail.com',NULL,NULL,NULL,1),(37,'0000080636','David Reynolds','reynolds30062@gmail.com',NULL,NULL,NULL,1),(38,'0000080637','David Reynolds','reynolds30062@gmail.com',NULL,NULL,NULL,1),(39,'0000080637','Seth Stowers','sstowers17@icloud.com',NULL,NULL,NULL,1),(40,'0000080663','David Reynolds','reynolds30062@gmail.com',NULL,NULL,NULL,1),(41,'0000080663','Seth Stowers','sstowers17@icloud.com',NULL,NULL,NULL,1),(44,'0000080636','Seth Stowers','sstowers17@icloud.com',NULL,NULL,NULL,1),(45,'0000080637','Sam Fellows','sam_fellows@pepperidgefarm.com',NULL,NULL,NULL,1),(46,'0000080637','Steve Grow','steve_grow@pepperidgefarm.com',NULL,NULL,NULL,1);

/*Table structure for table `role` */

CREATE TABLE `role` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `managePOD` tinyint(1) NOT NULL DEFAULT '0',
  `manageBID` tinyint(1) NOT NULL DEFAULT '0',
  `PODmissingDepotEmail` tinyint(1) NOT NULL DEFAULT '0',
  `BIDmanageBidsEmail` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `role` */

insert  into `role`(`id`,`superadmin`,`managePOD`,`manageBID`,`PODmissingDepotEmail`,`BIDmanageBidsEmail`) values ('BIDADMIN',0,1,1,0,1),('MGRADMIN',0,1,1,0,1),('PODADMIN',0,1,0,1,0),('PODBIDADMIN',0,1,1,1,0),('SUPER ADMIN',1,1,1,1,0);

/*Table structure for table `settings` */

CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skey` varchar(100) DEFAULT NULL,
  `svalue` varchar(100) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`id`,`skey`,`svalue`,`admin_email`) values (1,'FROM_EMAIL','POD Support<subscribe2mani@gmail.com>',NULL),(2,'APP_VERSION','1.0',NULL),(3,'FILTER_DAYS','30',NULL);

/*Table structure for table `shipment` */

CREATE TABLE `shipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `depot_id` varchar(50) NOT NULL,
  `ship_num` varchar(50) DEFAULT NULL,
  `delivery` varchar(50) DEFAULT NULL,
  `ship_to_customer` varchar(50) DEFAULT NULL,
  `upload_mode` varchar(10) DEFAULT NULL,
  `imei` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `upload_dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `const_depot` (`depot_id`),
  CONSTRAINT `const_depot` FOREIGN KEY (`depot_id`) REFERENCES `depot` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

/*Data for the table `shipment` */

insert  into `shipment`(`id`,`depot_id`,`ship_num`,`delivery`,`ship_to_customer`,`upload_mode`,`imei`,`email`,`upload_dt`) values (17,'00803651','222','333','444','MOBILE','0003669523323275',NULL,'2016-11-11 14:08:10'),(18,'00803652','432','111','234','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(19,'00803653','222','333','444','MOBILE','0003669523323275',NULL,NULL),(20,'00803653','222','333','444','MOBILE','0003669523323275',NULL,NULL),(21,'00803653','222','333','444','MOBILE','0003669523323275',NULL,NULL),(22,'00803652','432','111','234','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(23,'00803652','432','111','234','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(32,'00803654','432','111','234','MOBILE',NULL,NULL,NULL),(37,'00803651','222','333','444','MOBILE','0003669523323275',NULL,'2016-11-11 14:08:19'),(40,'00803652','432','111','234','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(41,'00803652','432','111','234','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(42,'00803652','432','111','234','MOBILE','0003669523323275',NULL,NULL),(43,'00803652','432','111','234','MOBILE','0003669523323275',NULL,NULL),(44,'00803652','432','111','234','MOBILE','0003669523323275',NULL,NULL),(45,'00803652','432','111','234','MOBILE','0003669523323275',NULL,NULL),(47,'00803652','432','111','234','MOBILE','0003669523323275',NULL,NULL),(50,'0000086936','0003442471','8069654645','0000086936','MOBILE','353321062446128',NULL,'2016-11-20 14:08:04'),(51,'00803651','0003626266','8075885034','00803651','MOBILE','0003669523323275',NULL,'2016-11-09 14:08:23'),(53,'0000080026','0003710616','8078051488','0000080026','MOBILE','861428030917703',NULL,'2016-11-21 14:07:41'),(56,'0000080636','0003626269','8075885022','0000080636','MOBILE','869627022581151',NULL,'2016-11-21 14:07:58'),(58,'0000080636','0003626270','8075885255','0000080636','MOBILE','869627022581151',NULL,'2016-11-22 14:13:34'),(59,'0000080663','0003726146','8078520661','0000080663','MOBILE','869627021993050',NULL,'2016-11-22 19:17:14'),(60,'0000080637','0003626266','8075885254','0000080637','MOBILE','869627022581151',NULL,'2016-11-23 22:25:55'),(61,'0000080636','0003626269','8075885022','0000080636','MOBILE','869627022581151',NULL,'2016-11-23 22:28:58'),(63,'0000080636','0003626270','8075885255','0000080636','MOBILE','869627022581151',NULL,'2016-11-24 17:00:34'),(64,'0000080637','0003626266','8075885254','0000080637','MOBILE','869627022581151',NULL,'2016-11-24 17:07:11'),(67,'0000080663','0003732034','8078735327','0000080663','MOBILE','869627021993050',NULL,'2016-11-29 14:16:34'),(68,'0000080637','0003626266','8075885254','0000080637','MOBILE','869627022581151',NULL,'2016-12-01 04:12:28'),(69,'0000080636','0003626270','8075885255','0000080636','MOBILE','869627022581151',NULL,'2016-12-01 04:14:21'),(71,'0000080637','0003626267','8075885035','0000080637','MOBILE','869627022581151',NULL,'2016-12-01 14:12:09'),(72,'0000080636','0003626269','8075885022','0000080636','MOBILE','869627022581151',NULL,'2016-12-01 14:18:24'),(75,'0000080636','0003735664','8078868046','0000080636','MOBILE','869627021993050',NULL,'2016-12-01 15:37:20'),(76,'0000080637','0003735659','8078868047','0000080637','MOBILE','869627021993050',NULL,'2016-12-01 15:37:22'),(77,'0000080637','0003626266','8075885254','0000080637','MOBILE','869627022581151',NULL,'2016-12-02 17:05:16'),(78,'0000080637','0003740279','8079009218','0000080637','MOBILE','869627022581151',NULL,'2016-12-05 18:25:07'),(82,'0000080636','0003744445','8079135481','0000080636','MOBILE','357754077774744',NULL,'2016-12-08 15:14:36'),(87,'0000080637','0003626266','8075885254','0000080637','MOBILE','869627022581151',NULL,'2016-12-12 21:52:26'),(94,'0000080663','0003755683','8079502838','0000080663','MOBILE','866738022560997',NULL,'2016-12-17 07:46:49');

/*Table structure for table `shipment_detail` */

CREATE TABLE `shipment_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` bigint(20) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `thumbnail_url` text,
  `file_url` text,
  PRIMARY KEY (`id`),
  KEY `const_shipment` (`shipment_id`),
  CONSTRAINT `const_shipment` FOREIGN KEY (`shipment_id`) REFERENCES `shipment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

/*Data for the table `shipment_detail` */

insert  into `shipment_detail`(`id`,`shipment_id`,`filename`,`file_type`,`filesize`,`thumbnail_url`,`file_url`) values (25,17,'20161027_2','png',61257,'222#333#444/20161027_2.png','222#333#444/20161027_2.png'),(26,17,'20161027_3','png',23287,'222#333#444/20161027_3.png','222#333#444/20161027_3.png'),(27,17,'Message','json',172,'222#333#444/Message.json','222#333#444/Message.json'),(28,18,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(29,18,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(30,18,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(31,19,'20161027_2','png',61257,'222#333#444/20161027_2.png','222#333#444/20161027_2.png'),(32,19,'20161027_3','png',23287,'222#333#444/20161027_3.png','222#333#444/20161027_3.png'),(33,19,'Message','json',172,'222#333#444/Message.json','222#333#444/Message.json'),(34,20,'20161027_2','png',61257,'222#333#444/20161027_2.png','222#333#444/20161027_2.png'),(35,20,'20161027_3','png',23287,'222#333#444/20161027_3.png','222#333#444/20161027_3.png'),(36,20,'Message','json',172,'222#333#444/Message.json','222#333#444/Message.json'),(37,21,'20161027_2','png',61257,'222#333#444/20161027_2.png','222#333#444/20161027_2.png'),(38,21,'20161027_3','png',23287,'222#333#444/20161027_3.png','222#333#444/20161027_3.png'),(39,21,'Message','json',172,'222#333#444/Message.json','222#333#444/Message.json'),(40,22,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(41,22,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(42,22,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(43,23,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(44,23,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(45,23,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(46,37,'20161027_2','png',61257,'222#333#444/20161027_2.png','222#333#444/20161027_2.png'),(47,37,'20161027_3','png',23287,'222#333#444/20161027_3.png','222#333#444/20161027_3.png'),(48,37,'Message','json',172,'222#333#444/Message.json','222#333#444/Message.json'),(49,40,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(50,40,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(51,40,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(52,41,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(53,41,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(54,41,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(55,42,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(56,42,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(57,42,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(58,43,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(59,43,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(60,43,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(61,44,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(62,44,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(63,44,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(64,45,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(65,45,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(66,45,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(67,47,'20161027_2','png',61257,'432#111#234/20161027_2.png','432#111#234/20161027_2.png'),(68,47,'20161027_3','png',23287,'432#111#234/20161027_3.png','432#111#234/20161027_3.png'),(69,47,'Message','json',172,'432#111#234/Message.json','432#111#234/Message.json'),(70,50,'Screenshot_2016-11-11-10-34-10','png',135372,'0003442471#8069654645#0000086936/Screenshot_2016-11-11-10-34-10.png','0003442471#8069654645#0000086936/Screenshot_2016-11-11-10-34-10.png'),(71,50,'message','json',120,'0003442471#8069654645#0000086936/message.json','0003442471#8069654645#0000086936/message.json'),(72,51,'Message','json',191,'0003626266#8075885034#00803651/Message.json','0003626266#8075885034#00803651/Message.json'),(73,51,'image1','JPG',1496994,'0003626266#8075885034#00803651/image1.JPG','0003626266#8075885034#00803651/image1.JPG'),(74,53,'International-Daffodil_&_467f0491-e7fb-49ba-9510-5c88df960507','jpg',510356,'0003710616#8078051488#0000080026/International-Daffodil_&_467f0491-e7fb-49ba-9510-5c88df960507.jpg','0003710616#8078051488#0000080026/International-Daffodil_&_467f0491-e7fb-49ba-9510-5c88df960507.jpg'),(75,53,'international-road_&_7aa53d35-ded7-4e6e-83f9-a62cf0c559f4','jpg',619975,'0003710616#8078051488#0000080026/international-road_&_7aa53d35-ded7-4e6e-83f9-a62cf0c559f4.jpg','0003710616#8078051488#0000080026/international-road_&_7aa53d35-ded7-4e6e-83f9-a62cf0c559f4.jpg'),(76,53,'message','json',120,'0003710616#8078051488#0000080026/message.json','0003710616#8078051488#0000080026/message.json'),(77,56,'IMG_20161121_174942-128597283','jpg',2085075,'0003626269#8075885022#0000080636/IMG_20161121_174942-128597283.jpg','0003626269#8075885022#0000080636/IMG_20161121_174942-128597283.jpg'),(78,56,'message','json',120,'0003626269#8075885022#0000080636/message.json','0003626269#8075885022#0000080636/message.json'),(79,58,'IMG_20161122_0913071070473579','jpg',3164528,'0003626270#8075885255#0000080636/IMG_20161122_0913071070473579.jpg','0003626270#8075885255#0000080636/IMG_20161122_0913071070473579.jpg'),(80,58,'message','json',120,'0003626270#8075885255#0000080636/message.json','0003626270#8075885255#0000080636/message.json'),(81,59,'20161122141626','jpg',1594978,'0003726146#8078520661#0000080663/20161122141626.jpg','0003726146#8078520661#0000080663/20161122141626.jpg'),(82,59,'message','json',120,'0003726146#8078520661#0000080663/message.json','0003726146#8078520661#0000080663/message.json'),(83,60,'20161123172537','jpg',1170240,'0003626266#8075885254#0000080637/20161123172537.jpg','0003626266#8075885254#0000080637/20161123172537.jpg'),(84,60,'message','json',120,'0003626266#8075885254#0000080637/message.json','0003626266#8075885254#0000080637/message.json'),(85,61,'20161123172739','jpg',842358,'0003626269#8075885022#0000080636/20161123172739.jpg','0003626269#8075885022#0000080636/20161123172739.jpg'),(86,61,'message','json',120,'0003626269#8075885022#0000080636/message.json','0003626269#8075885022#0000080636/message.json'),(87,63,'20161124120015','jpg',1111812,'0003626270#8075885255#0000080636/20161124120015.jpg','0003626270#8075885255#0000080636/20161124120015.jpg'),(88,63,'message','json',120,'0003626270#8075885255#0000080636/message.json','0003626270#8075885255#0000080636/message.json'),(89,64,'20161124120548','jpg',1818387,'0003626266#8075885254#0000080637/20161124120548.jpg','0003626266#8075885254#0000080637/20161124120548.jpg'),(90,64,'message','json',120,'0003626266#8075885254#0000080637/message.json','0003626266#8075885254#0000080637/message.json'),(91,67,'20161129091623','jpg',972940,'0003732034#8078735327#0000080663/20161129091623.jpg','0003732034#8078735327#0000080663/20161129091623.jpg'),(92,67,'message','json',120,'0003732034#8078735327#0000080663/message.json','0003732034#8078735327#0000080663/message.json'),(93,68,'20161130231204','jpg',1452341,'0003626266#8075885254#0000080637/20161130231204.jpg','0003626266#8075885254#0000080637/20161130231204.jpg'),(94,68,'message','json',120,'0003626266#8075885254#0000080637/message.json','0003626266#8075885254#0000080637/message.json'),(95,69,'20161130231307','jpg',1373060,'0003626270#8075885255#0000080636/20161130231307.jpg','0003626270#8075885255#0000080636/20161130231307.jpg'),(96,69,'message','json',120,'0003626270#8075885255#0000080636/message.json','0003626270#8075885255#0000080636/message.json'),(97,71,'20161201091145','jpg',1061653,'0003626267#8075885035#0000080637/20161201091145.jpg','0003626267#8075885035#0000080637/20161201091145.jpg'),(98,71,'message','json',120,'0003626267#8075885035#0000080637/message.json','0003626267#8075885035#0000080637/message.json'),(99,72,'20161201091706','jpg',1264484,'0003626269#8075885022#0000080636/20161201091706.jpg','0003626269#8075885022#0000080636/20161201091706.jpg'),(100,72,'message','json',120,'0003626269#8075885022#0000080636/message.json','0003626269#8075885022#0000080636/message.json'),(101,75,'20161201103707','jpg',1159029,'0003735664#8078868046#0000080636/20161201103707.jpg','0003735664#8078868046#0000080636/20161201103707.jpg'),(102,75,'message','json',120,'0003735664#8078868046#0000080636/message.json','0003735664#8078868046#0000080636/message.json'),(103,76,'20161201103604','jpg',1217093,'0003735659#8078868047#0000080637/20161201103604.jpg','0003735659#8078868047#0000080637/20161201103604.jpg'),(104,76,'message','json',120,'0003735659#8078868047#0000080637/message.json','0003735659#8078868047#0000080637/message.json'),(105,77,'20161202120455','jpg',1131414,'0003626266#8075885254#0000080637/20161202120455.jpg','0003626266#8075885254#0000080637/20161202120455.jpg'),(106,77,'message','json',120,'0003626266#8075885254#0000080637/message.json','0003626266#8075885254#0000080637/message.json'),(107,78,'20161205132447','jpg',1052414,'0003740279#8079009218#0000080637/20161205132447.jpg','0003740279#8079009218#0000080637/20161205132447.jpg'),(108,78,'message','json',120,'0003740279#8079009218#0000080637/message.json','0003740279#8079009218#0000080637/message.json'),(109,82,'20161208101421','jpg',2489254,'0003744445#8079135481#0000080636/20161208101421.jpg','0003744445#8079135481#0000080636/20161208101421.jpg'),(110,82,'message','json',120,'0003744445#8079135481#0000080636/message.json','0003744445#8079135481#0000080636/message.json'),(111,87,'20161212165211','jpg',996728,'0003626266#8075885254#0000080637/20161212165211.jpg','0003626266#8075885254#0000080637/20161212165211.jpg'),(112,87,'message','json',120,'0003626266#8075885254#0000080637/message.json','0003626266#8075885254#0000080637/message.json'),(113,94,'20161217024617','jpg',1140545,'0003755683#8079502838#0000080663/20161217024617.jpg','0003755683#8079502838#0000080663/20161217024617.jpg'),(114,94,'message','json',120,'0003755683#8079502838#0000080663/message.json','0003755683#8079502838#0000080663/message.json');

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `thumbnail_url` text,
  `image_url` text,
  `status` tinyint(1) NOT NULL,
  `role` varchar(100) NOT NULL,
  `shipperId` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`pwd`,`mobile`,`thumbnail_url`,`image_url`,`status`,`role`,`shipperId`) values (1,'Senthil','senthil.raju@excelblaze.com','123','123123123',NULL,NULL,1,'SUPER ADMIN',1),(2,'David','reynolds30062@gmail.com','david54321','123123123',NULL,NULL,1,'SUPER ADMIN',1),(3,'Niaz','mail2niazahamed@gmail.com','Passw0rd','123123123',NULL,NULL,1,'MGRADMIN',1),(4,'Mani','subscribe2mani@gmail.com','Passw0rd','123123123',NULL,NULL,1,'BIDADMIN',1),(10,'NiazTest2','dd.dd@excelblaze.com','123','123123123',NULL,NULL,0,'MGRADMIN',1),(11,'NiazTest3','dd.dd@excelblaze.com','123','123123123',NULL,NULL,0,'SUPER ADMIN',1),(12,'NiazTest4','dd.dd@excelblaze.com','123','123123123',NULL,NULL,0,'SUPER ADMIN',1),(13,'NiazTest005','dd.dd11@excelblaze.com','123','123123123',NULL,NULL,0,'SUPER ADMIN',1),(15,'Naveed','vnaveed@gmail.com','1234','9999999999',NULL,NULL,0,'SUPER ADMIN',1),(24,'Seth Stowers','sstowers17@icloud.com','sstowers54321','8768778786',NULL,NULL,0,'SUPER ADMIN',1),(26,'Seth Stowers','sstowers17@icloud.com','sstowers54321',NULL,NULL,NULL,0,'SUPER ADMIN',1),(27,'Sam Fellows','sam_fellows@pepperidgefarm.com','sam54321',NULL,NULL,NULL,0,'SUPER ADMIN',1),(29,'Scott Stowers','Sstowers@dtsolutions.net','scott54321',NULL,NULL,NULL,0,'SUPER ADMIN',1),(31,'Shari','Accounting@translytix.com','shari54321',NULL,NULL,NULL,0,'SUPER ADMIN',1),(32,'Matt Kramer','mkramer@dtsolutions.net','0518','',NULL,NULL,0,'SUPER ADMIN',1),(33,'Steve Grow','steve_grow@pepperidgefarm.com','steve54321',NULL,NULL,NULL,0,'SUPER ADMIN',1);

/*Table structure for table `BD_ReviewBids` */

DROP TABLE IF EXISTS `BD_ReviewBids`;

/*!50001 CREATE TABLE  `BD_ReviewBids`(
 `laneId` int(11) ,
 `fromCity` varchar(100) ,
 `toCity` varchar(100) ,
 `fromState` varchar(100) ,
 `toState` varchar(100) ,
 `distanceMiles` int(100) ,
 `refType` varchar(20) ,
 `WayLH` int(11) ,
 `expediteLH` int(11) ,
 `RTLH` int(11) ,
 `dropTrailors` tinyint(1) ,
 `bidUserId` int(11) ,
 `originPostalCode` mediumtext ,
 `destPostalCode` mediumtext ,
 `bidUserName` varchar(500) ,
 `active` tinyint(1) ,
 `carrierId` int(11) 
)*/;

/*Table structure for table `view_bd_lane` */

DROP TABLE IF EXISTS `view_bd_lane`;

/*!50001 CREATE TABLE  `view_bd_lane`(
 `fromCity` varchar(100) ,
 `fromState` varchar(100) ,
 `toCity` varchar(100) ,
 `toState` varchar(100) ,
 `policyDescription` varchar(500) ,
 `laneDateFrom` date ,
 `laneDateTo` date ,
 `distanceMiles` int(11) ,
 `policyId` int(11) ,
 `laneId` int(11) ,
 `annualLineHaul` int(11) ,
 `needRefridgerated` tinyint(1) ,
 `teamRate` tinyint(1) ,
 `bidSingleOneWayRate` tinyint(1) ,
 `bidSingleRoundTripRate` tinyint(1) ,
 `additionalInfo` varchar(1000) ,
 `shipperId` int(11) ,
 `truckType` varchar(100) ,
 `daysOfWeek` varchar(255) ,
 `bidTeamOneWayRate` tinyint(1) ,
 `bidTeamRoundTripRate` tinyint(1) ,
 `active` tinyint(1) 
)*/;

/*Table structure for table `view_depot_shipment` */

DROP TABLE IF EXISTS `view_depot_shipment`;

/*!50001 CREATE TABLE  `view_depot_shipment`(
 `depot_id` varchar(50) ,
 `depot_name` varchar(100) ,
 `id` bigint(20) ,
 `ship_num` varchar(50) ,
 `delivery` varchar(50) ,
 `ship_to_customer` varchar(50) ,
 `upload_mode` varchar(10) ,
 `imei` varchar(50) ,
 `email` varchar(100) ,
 `upload_dt` datetime ,
 `search` varchar(477) 
)*/;

/*Table structure for table `view_depot_user` */

DROP TABLE IF EXISTS `view_depot_user`;

/*!50001 CREATE TABLE  `view_depot_user`(
 `depot_id` varchar(50) ,
 `depot_name` varchar(100) ,
 `user_name` varchar(50) ,
 `user_email` varchar(100) ,
 `user_active_status` tinyint(1) ,
 `depot_user_id` int(11) ,
 `user_mobile` varchar(30) ,
 `search` varchar(357) 
)*/;

/*Table structure for table `view_user_role` */

DROP TABLE IF EXISTS `view_user_role`;

/*!50001 CREATE TABLE  `view_user_role`(
 `id` int(11) ,
 `name` varchar(50) ,
 `email` varchar(100) ,
 `pwd` varchar(100) ,
 `mobile` varchar(30) ,
 `thumbnail_url` text ,
 `image_url` text ,
 `status` tinyint(1) ,
 `role` varchar(100) ,
 `shipperId` int(11) ,
 `roleId` varchar(100) ,
 `superadmin` tinyint(1) ,
 `managePOD` tinyint(1) ,
 `manageBID` tinyint(1) ,
 `PODmissingDepotEmail` tinyint(1) ,
 `BIDmanageBidsEmail` tinyint(1) 
)*/;

/*View structure for view BD_ReviewBids */

/*!50001 DROP TABLE IF EXISTS `BD_ReviewBids` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`poduser`@`%` SQL SECURITY DEFINER VIEW `BD_ReviewBids` AS select `bid`.`laneId` AS `laneId`,`bid`.`fromCity` AS `fromCity`,`bid`.`toCity` AS `toCity`,`bid`.`fromState` AS `fromState`,`bid`.`toState` AS `toState`,`bid`.`distanceMiles` AS `distanceMiles`,`bid`.`refType` AS `refType`,`bid`.`WayLH` AS `WayLH`,`bid`.`expediteLH` AS `expediteLH`,`bid`.`RTLH` AS `RTLH`,`bid`.`dropTrailors` AS `dropTrailors`,`bid`.`bidUserId` AS `bidUserId`,`bid`.`originPostalCode` AS `originPostalCode`,`bid`.`destPostalCode` AS `destPostalCode`,`biduser`.`bidUserName` AS `bidUserName`,`biduser`.`active` AS `active`,`biduser`.`id` AS `carrierId` from (`BD_Bid` `bid` join `BD_BidUser` `biduser`) */;

/*View structure for view view_bd_lane */

/*!50001 DROP TABLE IF EXISTS `view_bd_lane` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`poduser`@`%` SQL SECURITY DEFINER VIEW `view_bd_lane` AS select `fromDest`.`city` AS `fromCity`,`fromDest`.`state` AS `fromState`,`toDest`.`city` AS `toCity`,`toDest`.`state` AS `toState`,`policy`.`description` AS `policyDescription`,`lane`.`laneDateFrom` AS `laneDateFrom`,`lane`.`laneDateTo` AS `laneDateTo`,`lane`.`distanceMiles` AS `distanceMiles`,`lane`.`policyId` AS `policyId`,`lane`.`laneId` AS `laneId`,`lane`.`annualLineHaul` AS `annualLineHaul`,`lane`.`needRefridgerated` AS `needRefridgerated`,`lane`.`teamRate` AS `teamRate`,`lane`.`bidSingleOneWayRate` AS `bidSingleOneWayRate`,`lane`.`bidSingleRoundTripRate` AS `bidSingleRoundTripRate`,`lane`.`additionalInfo` AS `additionalInfo`,`lane`.`shipperId` AS `shipperId`,`lane`.`truckType` AS `truckType`,`lane`.`daysOfWeek` AS `daysOfWeek`,`lane`.`bidTeamOneWayRate` AS `bidTeamOneWayRate`,`lane`.`bidTeamRoundTripRate` AS `bidTeamRoundTripRate`,`lane`.`active` AS `active` from (((`BD_Lane` `lane` join `BD_Destination` `fromDest` on((`fromDest`.`id` = `lane`.`fromDestinationId`))) join `BD_Destination` `toDest` on((`toDest`.`id` = `lane`.`toDestinationId`))) join `BD_ShipperPolicy` `policy` on((`policy`.`policyId` = `lane`.`policyId`))) */;

/*View structure for view view_depot_shipment */

/*!50001 DROP TABLE IF EXISTS `view_depot_shipment` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`poduser`@`%` SQL SECURITY DEFINER VIEW `view_depot_shipment` AS select `d`.`id` AS `depot_id`,`d`.`name` AS `depot_name`,`s`.`id` AS `id`,`s`.`ship_num` AS `ship_num`,`s`.`delivery` AS `delivery`,`s`.`ship_to_customer` AS `ship_to_customer`,`s`.`upload_mode` AS `upload_mode`,`s`.`imei` AS `imei`,`s`.`email` AS `email`,`s`.`upload_dt` AS `upload_dt`,concat_ws('~',`d`.`id`,`d`.`name`,`s`.`id`,`s`.`ship_num`,`s`.`delivery`,`s`.`ship_to_customer`,`s`.`email`,`s`.`imei`) AS `search` from (`depot` `d` join `shipment` `s` on((`d`.`id` = `s`.`depot_id`))) */;

/*View structure for view view_depot_user */

/*!50001 DROP TABLE IF EXISTS `view_depot_user` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`poduser`@`%` SQL SECURITY DEFINER VIEW `view_depot_user` AS select `d`.`id` AS `depot_id`,`d`.`name` AS `depot_name`,`du`.`name` AS `user_name`,`du`.`email` AS `user_email`,`du`.`status` AS `user_active_status`,`du`.`id` AS `depot_user_id`,`du`.`mobile` AS `user_mobile`,concat(`d`.`id`,'||',`d`.`name`,'||',`du`.`name`,'||',`du`.`email`,'||',`du`.`status`,'||',`du`.`id`,'||',`du`.`mobile`) AS `search` from (`depot` `d` join `depot_user` `du`) where (`d`.`id` = `du`.`depot_id`) order by `d`.`id`,`du`.`depot_id` */;

/*View structure for view view_user_role */

/*!50001 DROP TABLE IF EXISTS `view_user_role` */;
/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`poduser`@`%` SQL SECURITY DEFINER VIEW `view_user_role` AS select `u`.`id` AS `id`,`u`.`name` AS `name`,`u`.`email` AS `email`,`u`.`pwd` AS `pwd`,`u`.`mobile` AS `mobile`,`u`.`thumbnail_url` AS `thumbnail_url`,`u`.`image_url` AS `image_url`,`u`.`status` AS `status`,`u`.`role` AS `role`,`u`.`shipperId` AS `shipperId`,`r`.`id` AS `roleId`,`r`.`superadmin` AS `superadmin`,`r`.`managePOD` AS `managePOD`,`r`.`manageBID` AS `manageBID`,`r`.`PODmissingDepotEmail` AS `PODmissingDepotEmail`,`r`.`BIDmanageBidsEmail` AS `BIDmanageBidsEmail` from (`users` `u` join `role` `r` on((`u`.`role` = `r`.`id`))) where (`u`.`status` = 1) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
